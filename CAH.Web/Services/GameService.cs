﻿using CAH.GameLogic;
using CAH.GameLogic.Models;
using CAH.GameLogic.TransferObjects;
using CAH.Web.TransferObjects;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CAH.Web.Services
{
    public class GameService
    {
        private readonly List<GameState> _games = new List<GameState>();
        private readonly ConcurrentDictionary<string, SocketHolder> _connections = new ConcurrentDictionary<string, SocketHolder>();
        private readonly Notifier _notifier;
        private readonly JsonSerializerSettings serializerSettings =
            new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() };

        public GameService()
        {
            _notifier = CreateNotifier();
        }

        public IEnumerable<GameInfo> GetGameInfo() => _games.Where(x => !x.IsHidden).Select(x => x.GameInfo);

        public string AddConnection(WebSocket webSocket)
        {
            var id = Guid.NewGuid().ToString();
            var socketHolder = new SocketHolder(webSocket, id, RemoveConnection);
            if (!_connections.TryAdd(id, socketHolder)) socketHolder.Dispose();

            return id;
        }

        public void RemoveConnection(string id)
        {
            if(_connections.TryRemove(id, out var socket)) socket.Dispose();
            var game = _games.FirstOrDefault(x => x.HasPlayerWithId(id));
            if (game != null)
            {
                var (shouldStartNewRound, shouldRevealCards) = game.RemovePlayer(id);
                if(shouldStartNewRound)
                {
                    game.StartNewRound();
                    return;
                }

                if(shouldRevealCards)
                {
                    game.RevealCards();
                }
            }
        }

        public string CreateGame(GameDescriptor gameDescriptor, IEnumerable<WhiteCard> whiteCards, IEnumerable<BlackCard> blackCards)
        {
            var id = Guid.NewGuid().ToString();
            _games.Add(new GameState(gameDescriptor.GameName, id, gameDescriptor.IsHidden, blackCards, whiteCards,
                gameDescriptor.GetRuleset(), _notifier));

            return id;
        }

        private Notifier CreateNotifier()
        {
            var notifier = new Notifier();
            var props = notifier.GetType().GetProperties();
            foreach(var prop in props)
            {
                var typeArgs = prop.PropertyType.GetGenericArguments();
                if(typeArgs.Count() == 1)
                {
                    if(typeArgs.First() == typeof(string))
                    {
                        Action<string> removeGame = gameId => RemoveGame(gameId);
                        prop.SetValue(notifier, removeGame);
                    }
                    else
                    {
                        Action<Player> singleArgument = async p => await SendMessageAsync(p.Id, new { type = prop.Name });
                        prop.SetValue(notifier, singleArgument);
                    }
                }
                else
                {
                    Action<Player, object> doubleArgument = async (p, data) => await SendMessageAsync(p.Id, new { type = prop.Name, data });
                    prop.SetValue(notifier, doubleArgument);
                }
            }

            return notifier;
        }

        public async Task HandleMessage(string message, string playerId)
        {
            JObject json = null;

            try
            {
                json = JObject.Parse(message);
            }
            catch (Exception) { /* log? */ }

            var game = GetGameById(json.Value<string>("gameId"));
            if(game == null)
            {
                await SendMessageAsync(playerId, new { type = "Error", data = "Game does not exist" });
                return;
            }

            switch (json["type"].Value<string>())
            {
                case "join":
                    await HandleJoinAsync(json, game, playerId);
                    break;
                case "chat":
                    HandleChatMessage(json, game, "chat");
                    break;
                case "cardsSelected":
                    await HandleCardsSelectedAsync(json, game, playerId);
                    break;
                case "winnerSelected":
                    HandleWinnerSelected(json, game);
                    break;
                case "discardHand":
                    HandleDiscardHand(game, playerId);
                    break;
                case "pong":
                    _connections[playerId]?.ResetNoPongCounter();
                    break;
            }
        }

        private void HandleDiscardHand(GameState game, string playerId)
        {
            game.DiscardHand(playerId);
        }

        private void HandleWinnerSelected(JObject json, GameState game)
        {
            string winnerId = json.Value<string>("data");
            if(game.SelectWinner(winnerId))
            {
                game.StartNewRound();
            }
        }

        private async Task HandleCardsSelectedAsync(JObject json, GameState game, string playerId)
        {
            try
            {
                var cardIds = json["data"].Values<int>().ToArray();
                if (game.SelectCards(playerId, cardIds))
                {
                    game.RevealCards();
                }
            }
            catch (InvalidOperationException ex)
            {
                await SendMessageAsync(playerId, new { type = "Error", data = ex.Message });
            }
        }

        private GameState GetGameById(string id) => _games.FirstOrDefault(x => x.Id == id);

        private void RemoveGame(string gameId)
        {
            var game = GetGameById(gameId);
            if(game != null)
            {
                _games.Remove(game);
            }
        }

        private async Task SendMessageAsync(string playerId, object message)
        {
            try
            {
                var webSocket = _connections[playerId];
                var json = JsonConvert.SerializeObject(message, serializerSettings);
                await webSocket.SendAsync(json);
            }
            catch (WebSocketException)
            {
                RemoveConnection(playerId);
            }
            catch (KeyNotFoundException) { }
        }

        private async Task HandleJoinAsync(JObject json, GameState game, string playerId)
        {
            string name = json.Value<string>("data")?.Trim();
            if (string.IsNullOrWhiteSpace(name))
            {
                await SendMessageAsync(playerId, new { type = "Error", data = "This is not a valid name" });
                return;
            }

            if(name.Length > 20)
            {
                await SendMessageAsync(playerId, new { type = "Error", data = "Name is too long. Max 20 characters are allowed" });
                return;
            }

            await SendMessageAsync(playerId, new { type = "IdAssigned", data = playerId });
            if (game.AddPlayer(playerId, name))
            {
                game.StartNewRound();
            }
        }

        private void HandleChatMessage(JObject json, GameState game, string messageType)
        {
            string text = json["data"].Value<string>("text");
            string name = json["data"].Value<string>("name");
            game.MessagePlayers(messageType, text, name);
        }
    }
}
