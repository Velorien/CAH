﻿namespace CAH.GameLogic.Models
{
    public class WhiteCard
    {
        public WhiteCard(int id, string text)
        {
            Id = id;
            Text = text;
        }

        public int Id { get; }

        public string Text { get; }
    }
}
